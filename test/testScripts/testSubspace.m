function [output] = testSubspace(A,B,E,jcounter,s_ma,m,tangential,VCRKSM,wait,iter,jCol_inp)
% This function is for testing the subspace and which structure of building
% a Krylov subspace is the most efficient way in case of SISO, block MIMO
% and MIMO tangential
% hier kommen die subspaces zusammengefasst


%% Declare Inputs

% set global variables (results of this function are visible in CRKSM-function)
global Vstand Vcascad Vrk
persistent Aold V1

% create Opts-struct


jCol_inp
%% Building the subspaces parallel to CRKSM
if wait == 0
% build standard Krylov subspace                                             
if jcounter == 1 && iter < 4                             
        % calculate subspace of first iteration with arnoldi
        if tangential == 0
            [Vstand] = arnoldi(E,A,B,s_ma(1,1));
        else
            [Vstand] = arnoldi(E,A,B,s_ma(1,1),s_ma(2:m+1,1));
        end
        Vstand = real(Vstand);
elseif jcounter == 2 && iter < 4
        % calculate subspace of first iteration with arnoldi
        % if first and second shift is a complex conjugated pair
        if tangential == 0
            [Vstand] = arnoldi(E,A,B,s_ma(1,1:2));
        else
            [Vstand] = arnoldi(E,A,B,s_ma(1,1:2),s_ma(2:m+1,1:2));
        end
        Vstand = real(Vstand);
else
    % m columns of current shift in ii_th iteration
    % calculate new direction(s)
    Anew = (A-s_ma(1,jcounter)*E);
    if tangential == 1
        if ~isreal(s_ma(2:m+1,jcounter))
            treal = real(s_ma(2:m+1,jcounter));
            timag = imag(s_ma(2:m+1,jcounter));
            rhs_real = B*treal;
            rhs_imag = B*timag;
            vnew_real = solveLse(Anew,rhs_real);
            vnew_imag = solveLse(Anew,rhs_imag);
            Vstand = [Vstand vnew_real];
            Vstand = [Vstand vnew_imag];
            
        else
            rhs = B*s_ma(2:m+1,jcounter);
            vnew = solveLse(Anew,rhs);
            if ~isreal(vnew)
                vnew1 = real(vnew);
                Vstand = [Vstand vnew1];
                vnew2 = imag(vnew);
                Vstand = [Vstand vnew2];    
             else
                % enlarge subspace and make it real
                Vstand = [Vstand vnew];
            end
        end
    else
        rhs = B;
        vnew = solveLse(Anew,rhs);
        if ~isreal(vnew)
            vnew1 = real(vnew);
            Vstand = [Vstand vnew1];
            vnew2 = imag(vnew);
            Vstand = [Vstand vnew2];    
        else
            % enlarge subspace and make it real
             Vstand = [Vstand vnew];
        end
    end
end



% build cascaded subspace                  
if (jcounter == 1 || jcounter == 2) && iter < 4                           
     Vcascad = Vstand;
    % set first rhs
    if jcounter == 1
        [Aold] = arnoldi(E,A,B,s_ma(1,1));
        Aold = Aold(:,size(Aold,2)-m+1:size(Aold,2));
    else
        [Aold] = arnoldi(E,A,B,s_ma(1,1:2));
        Aold = Aold(:,size(Aold,2)-m+1:size(Aold,2));
    end  
    
     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%      if jcounter == 1  
%          [V1] = arnoldi(E,A,B,s_ma(1,1));
%      else
%          [V1] = arnoldi(E,A,B,s_ma(1,1:2));
%      end
%      
     
     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
else
    if tangential == 1
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % first built cascaded block
%         rhs = E*V1(:,size(V1,2)-m+1:size(V1,2));
%         vnew_star = solveLse(Anew,rhs);
%         if ~isreal(vnew_star)
%             vnew1_star = real(vnew_star);
%             V1 = [V1 vnew1_star];
%             vnew2_star = imag(vnew_star);
%             V1 = [V1 vnew2_star];    
%         else
%             V1 = [V1 vnew_star];
%         end
        % bis hier her einfach nur block
        
        %vnew = vnew_star*s_ma(2:m+1,jcounter);
%         if ~isreal(s_ma(2:m+1,jcounter))
%             treal = real(s_ma(2:m+1,jcounter));
%             timag = imag(s_ma(2:m+1,jcounter));
%             vnew_real = vnew_star*treal;
%             vnew_imag = vnew_star*timag;
%             Vcascad = [Vcascad vnew_real];
%             Vcascad = [Vcascad vnew_imag];
%             
%         else
%             vnew = vnew_star*s_ma(2:m+1,jcounter);
%             if ~isreal(vnew)
%                 vnew1 = real(vnew);
%                 Vcascad = [Vcascad vnew1];
%                 vnew2 = imag(vnew);
%                 Vcascad = [Vcascad vnew2];    
%             else
%                 Vcascad = [Vcascad vnew];
%             end
%         end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        rhs = E*Aold;
        vnew_tan = solveLse(Anew,rhs);
        Aold = vnew_tan;
        vnew = vnew_tan*s_ma(2:m+1,jcounter);
        
        if ~isreal(vnew)
            vnew1 = real(vnew);
            Vcascad = [Vcascad vnew1];
            vnew2 = imag(vnew);
            Vcascad = [Vcascad vnew2];    
        else
            Vcascad = [Vcascad vnew];
        end
        
    else
        % set rhs with last direction
        rhs = E*Vcascad(:,size(Vcascad,2)-m+1:size(Vcascad,2));
        vnew = solveLse(Anew,rhs);
        if ~isreal(vnew)
            vnew1 = real(vnew);
            Vcascad = [Vcascad vnew1];
            vnew2 = imag(vnew);
            Vcascad = [Vcascad vnew2];    
        else
            Vcascad = [Vcascad vnew];
        end
    end        
end

% build basis with rk
sys = loadSss('iss');
if tangential == 1
    if iter <= size(s_ma,2)
        if mod(jcounter,2) == 0
            [sysr, Vrk, ~] = rk(sys,s_ma(1,1:jcounter),s_ma(2:m+1,1:jcounter));
        else
            [sysr, Vrk, ~] = rk(sys,s_ma(1,1:jcounter+1),s_ma(2:m+1,1:jcounter+1));
        end
    else
        [sysr, Vrk, ~] = rk(sys,s_ma(1,:),s_ma(2:m+1,:));
    end
else
    if iter <= size(s_ma,2)
        if mod(jcounter,2) == 0
            [sysr, Vrk, ~] = rk(sys,s_ma(1,1:jcounter));
        else
            [sysr, Vrk, ~] = rk(sys,s_ma(1,1:jcounter+1));
        end
    else
         [sysr, Vrk, ~] = rk(sys,s_ma(1,:));
    end
end
clear sysr
end

% create output
output = struct('rkVCRKSM',[], 'rkVstand',[], 'rkVcascad',[],'rkVrk',[],'VCRKSM',[], 'Vstand',[], 'Vcascad',[],'Vrk',[],...
                    'alpha1',[], 'alpha2',[], 'alpha3',[], 'alpha4',[], 'alpha5',[], 'alpha6',[]);

output.VCRKSM = VCRKSM;     output.Vstand = Vstand;     output.Vcascad = Vcascad;
output.Vrk = Vrk;   
                
% rank of subspaces
output.rkVCRKSM = rank(VCRKSM);  output.rkVcascad = rank(Vcascad);
output.rkVstand = rank(Vstand);  output.rkVrk = rank(Vrk);

% angle between subspaces
output.alpha1 = subspace(VCRKSM,Vstand);    output.alpha2 = subspace(Vcascad,Vstand);
output.alpha3 = subspace(VCRKSM,Vcascad);   output.alpha4 = subspace(Vrk,Vstand);
output.alpha5 = subspace(Vrk,Vcascad);      output.alpha6 = subspace(Vrk,VCRKSM);  


end













